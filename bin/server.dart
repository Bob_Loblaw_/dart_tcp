import 'dart:io';

void main(List<String> args) async {
  final socket = await ServerSocket.bind('127.0.0.1', 42069);
  socket.listen((event) {
    handleConnection(event);
  });
}

void handleConnection(Socket client) async {
  client.listen(
    (event) async {
      final req = String.fromCharCodes(event).toLowerCase();
      if (req.contains('http')) {
        client.writeAll([
          'HTTP/1.1 1 OK\r\n',
          '\n',
          '\nHello World!\n',
        ]);
        client.close();
      } else {
        client.write('ECHO: $req');
      }
    },
    onDone: () {
      client.close();
    },
    onError: (_) {
      client.close();
    },
  );
}
